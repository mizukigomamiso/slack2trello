## 概要
特定のSlackリアクションがWorkSpace内のメッセージにaddされると実行されるLambdaスクリプトです。  
メッセージの内容をタイトルとして、Trelloのリストにカードが追加されます。  
トリガーとなるリアクションとカード追加先のTrelloリストはいずれもserverless.yml内で指定します。

## 実行環境
- Python3.6

## 準備
- Trello APIのkeyとtoken(認証に必要な情報)を取得
```
・key
https://trello.com/app-key にアクセス
・token
https://trello.com/1/authorize?key=<上で取得したkey>&name=&expiration=never&response_type=token&scope=read,write　にアクセス
```

- TrelloのリストIDを取得
```
vi get_trello_ids.py  (ユーザー名・ボード名・リスト名を入力)
python get_trello_ids.py
```

- configファイルの設定
```
cp serverless_sample.yml serverless.yml
vi serverless.yml
```

# 必要なモジュールのインストール
- pipでのインストール
```
pip install -t ./ -r requirements.txt
```

- Serverless Frameworkのインストール
```
npm install -g serverless
```

import requests

# こちらへアクセスしてAPI keyを取得してください。→ https://trello.com/app-key
# ↓のURLをブラウザで開くと、Tokenが取得できます
# ... https://trello.com/1/authorize?key=<上で取得したKey>&name=&expiration=never&response_type=token&scope=read,write

user = ''  # Username(Trelloの「@〜〜」の@より後の部分)
key = ''  # API key
token = ''  # Token

board_name = ''  # カードを追加するボードの名前
list_name = ''  # カードを追加するリストの名前

## 以下は入力不要です。


def main():
    try:
        # ボードのIDを取得
        boards = requests.get(f'https://api.trello.com/1/members/{user}/boards?key={key}&token={token}&fields=name').json()
        board_id = list(filter(lambda x:x["name"] == board_name, boards))[0]["id"]

        # リストのIDを取得
        lists = requests.get(f'https://trello.com/1/boards/{board_id}/lists?key={key}&token={token}&fields=name').json()
        list_id = list(filter(lambda x:x["name"] == list_name, lists))[0]["id"]
        print(f'list ID: {list_id}')
    except Exception as e:
        print("%s: %s" % (type(e), e))
        raise

main()

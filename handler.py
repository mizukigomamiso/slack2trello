import json
import logging
import os
import requests

from logging import getLogger, INFO

logger = getLogger()
logger.setLevel(logging.INFO)

user = os.getenv('USER')
key = os.getenv('KEY')
token = os.getenv('TOKEN')
list = os.getenv('LIST')  # from slack
trigger_reaction = os.getenv('REACTION')

slack_token = os.getenv('SLACK_TOKEN')


# トリガーとなるリアクションのついたメッセージの内容を取得
def get_message(ch_id, ts):
    try:
        url = 'https://slack.com/api/conversations.history'
        payload = {
            "token": slack_token,
            "channel": ch_id,
            "latest": str(float(ts) + 1.0),  # 取得する投稿時間の範囲を対象投稿の少し後にする
            "limit": 1
        }

        res = requests.get(
            url,
            params=payload
        )
        res.raise_for_status()
        js = res.json()
        message = js["messages"][0]["text"].replace('&gt;', '').replace('&lt;', '')  # HTMLの特殊文字を削除
        return message
    except Exception as e:
        logger.error("%s: %s" % (type(e), e))


# Trelloの指定リストにカードを追加
def post_trello(title):
    try:
        url = f'https://trello.com/1/cards?key={key}&token={token}&idList={list}&name={title}'
        res = requests.post(url)
        res.raise_for_status()
    except Exception as e:
        logger.error("%s: %s" % (type(e), e))


def main(event, context):
    try:
        logger.info(event)
        js_eve = json.loads(event["body"])["event"]

        # Trelloカード追加のトリガーとなるリアクションかどうか判定
        if js_eve["reaction"] == trigger_reaction:
            ch_id = js_eve["item"]["channel"]  # 投稿にリアクションがついたチャンネルのID
            ts = js_eve["item"]["ts"]  # リアクションがついた投稿のタイムスタンプ
            text = get_message(ch_id, ts)  # 対象の投稿の情報を取得する
            post_trello(text)  # 投稿内容をタイトルにしたカードをTrelloに追加

        res = {
            "statusCode": 200,
            "body": '',
            "headers": {}
        }
        return res
    except Exception as e:
        logger.error("%s: %s" % (type(e), e))
